package ru.mtumanov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.user.UserLoginRq;
import ru.mtumanov.tm.dto.request.user.UserLogoutRq;
import ru.mtumanov.tm.dto.response.user.UserLoginRs;
import ru.mtumanov.tm.dto.response.user.UserLogoutRs;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return IEndpoint.newInstance(NAME, PART, IAuthEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(final String host, final String port) {
        return IEndpoint.newInstance(host, port, NAME, PART, IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserLoginRs login(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserLoginRq request);

    @NotNull
    @WebMethod
    UserLogoutRs logout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserLogoutRq request);

}

package ru.mtumanov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.project.*;
import ru.mtumanov.tm.dto.response.project.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return IEndpoint.newInstance(NAME, PART, IProjectEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(final String host, final String port) {
        return IEndpoint.newInstance(host, port, NAME, PART, IProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdRs projectChangeStatusById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectChangeStatusByIdRq request);

    @NotNull
    @WebMethod
    ProjectClearRs projectClear(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectClearRq request);

    @NotNull
    @WebMethod
    ProjectCompleteByIdRs projectCompleteById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectCompleteByIdRq request);

    @NotNull
    @WebMethod
    ProjectCreateRs projectCreate(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectCreateRq request);

    @NotNull
    @WebMethod
    ProjectListRs projectList(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectListRq request);

    @NotNull
    @WebMethod
    ProjectRemoveByIdRs projectRemoveById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectRemoveByIdRq request);

    @NotNull
    @WebMethod
    ProjectShowByIdRs projectShowById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectShowByIdRq request);

    @NotNull
    @WebMethod
    ProjectStartByIdRs projectStartById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectStartByIdRq request);

    @NotNull
    @WebMethod
    ProjectUpdateByIdRs projectUpdateById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectUpdateByIdRq request);

}

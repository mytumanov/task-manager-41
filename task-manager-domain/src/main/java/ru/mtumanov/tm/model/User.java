package ru.mtumanov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.enumerated.Role;

import javax.persistence.*;

@Entity
@Table(name = "tm_user")
@Getter
@Setter
public final class User extends AbstractModel {

    @NotNull
    @Column(nullable = false)
    private String login;

    @NotNull
    @Column(nullable = false, name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "mid_name")
    private String middleName;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column(nullable = false)
    private boolean locked = false;

}

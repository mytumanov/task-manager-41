package ru.mtumanov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    @Column(nullable = false, name = "user_id")
    protected String userId;

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof AbstractUserOwnedModel)) {
            return false;
        }
        AbstractUserOwnedModel abstractUserOwnedModel = (AbstractUserOwnedModel) o;
        return Objects.equals(userId, abstractUserOwnedModel.userId)
                && super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(userId);
    }

}

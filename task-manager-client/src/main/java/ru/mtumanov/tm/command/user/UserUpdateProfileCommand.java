package ru.mtumanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.user.UserUpdateRq;
import ru.mtumanov.tm.dto.response.user.UserUpdateRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "update current user profile";
    }

    @Override
    @NotNull
    public String getName() {
        return "update-user-profile";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateRq request = new UserUpdateRq(getToken(), firstName, lastName, middleName);
        @NotNull final UserUpdateRs response = getUserEndpoint().userUpdate(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}

package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.project.ProjectShowByIdRq;
import ru.mtumanov.tm.dto.response.project.ProjectShowByIdRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Show project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRq request = new ProjectShowByIdRq(getToken(), id);
        @NotNull final ProjectShowByIdRs response = getProjectEndpoint().projectShowById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
        showProject(response.getProject());
    }

}

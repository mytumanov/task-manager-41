package ru.mtumanov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.ISessionRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.ISessionService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.EntityEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.Session;

import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @NotNull
    public List<Session> findAll(@Nullable final String userId) throws AbstractException {
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAllUserId(userId);
        }
    }

    @Override
    @NotNull
    public Session add(@Nullable final String userId, @Nullable final Session model) throws AbstractException {
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        if (model == null)
            throw new EntityEmptyException(Session.class.getName());
        model.setUserId(userId);
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
                sessionRepository.add(model);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return model;
    }


    @Override
    @NotNull
    public Session findOneById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        if (id.isEmpty() || id == null)
            throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findOneById(userId, id);
        }
    }

    @Override
    public void remove(@Nullable final Session model) throws AbstractException {
        if (model == null)
            throw new EntityEmptyException(Session.class.getName());
        removeById(model.getUserId(), model.getId());
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) throws AbstractException {
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        if (id.isEmpty() || id == null)
            throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
                sessionRepository.removeById(userId, id);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    @NotNull
    public List<Session> findAll() {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAll();
        }
    }

}

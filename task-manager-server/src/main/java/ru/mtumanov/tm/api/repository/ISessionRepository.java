package ru.mtumanov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, user_id, created, role) VALUES (#{id}, #{userId}, #{created}, #{role})")
    void add(@NotNull Session model);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clear(@Param("userId") @NotNull String userId);

    @Select("SELECT EXISTS(SELECT 1 FROM tm_session WHERE user_id = #{userId} AND id = #{id})")
    boolean existById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @NotNull
    @Select("SELECT id, user_id, created, role FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Session> findAllUserId(@Param("userId") @NotNull String userId);

    @NotNull
    @Select("SELECT id, user_id, created, role FROM tm_session")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Session> findAll();

    @NotNull
    @Select("SELECT id, user_id, created, role FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    Session findOneById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT COUNT(1) FROM tm_session WHERE user_id = #{userId}")
    int getSize(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    void removeById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Update("UPDATE tm_session SET created = #{created}," +
            "role = #{role}," +
            "user_id = #{userId}" +
            "WHERE id = #{id}")
    void update(@NotNull Session model);

}

package ru.mtumanov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.mtumanov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, created, name, description, status, user_id) VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId})")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void add(@NotNull Project model);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clear(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM tm_project")
    void clearAll();

    @Select("SELECT EXISTS(SELECT 1 FROM tm_project WHERE user_id = #{userId} AND id = #{id})")
    boolean existById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAllUserId(@Param("userId") @NotNull String userId);

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id FROM tm_project")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAll();

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<Project> findAllComporator(@NotNull SelectStatementProvider selectStatement);

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    Project findOneById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT COUNT(1) FROM tm_project WHERE user_id = #{userId}")
    int getSize(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Update("UPDATE tm_project SET created = #{created}," +
            "name = #{name}," +
            "description = #{description}," +
            "status = #{status}," +
            "user_id = #{userId} " +
            "WHERE id = #{id}")
    void update(@NotNull Project model);

}

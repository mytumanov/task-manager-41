package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password) throws AbstractException;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull String email) throws AbstractException;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull Role role) throws AbstractException;

    @NotNull
    User findByLogin(@NotNull String login) throws AbstractException;

    @NotNull
    User findByEmail(@NotNull String email) throws AbstractException;

    @NotNull
    User findById(@NotNull String id) throws AbstractException;

    @NotNull
    User removeByLogin(@NotNull String login) throws AbstractException;

    @NotNull
    User removeByEmail(@NotNull String email) throws AbstractException;

    @NotNull
    User setPassword(@NotNull String id, @NotNull String password) throws AbstractException;

    @NotNull
    User userUpdate(@NotNull String id, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName) throws AbstractException;

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

    void lockUserByLogin(@NotNull String login) throws AbstractException;

    void unlockUserByLogin(@NotNull String login) throws AbstractException;

}
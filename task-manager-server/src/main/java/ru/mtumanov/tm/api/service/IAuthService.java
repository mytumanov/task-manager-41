package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Session;
import ru.mtumanov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws AbstractException;

    void logout(@Nullable Session session) throws AbstractException;

    @NotNull
    Session validateToken(@Nullable String token) throws AbstractException;

}

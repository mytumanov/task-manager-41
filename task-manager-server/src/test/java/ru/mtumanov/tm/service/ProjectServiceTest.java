package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.marker.DBCategory;
import ru.mtumanov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectRepository projectRepository = connectionService.getConnection().getMapper(IProjectRepository.class);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private static final List<Project> projectList = new ArrayList<>();

    @Before
    public void initRepository() throws Exception {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project name: " + i);
            project.setDescription("Project description: " + i);
            if (i <= 5)
                project.setUserId(USER_ID_1);
            else
                project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @After
    public void clearRepository() throws Exception {
        projectRepository.clearAll();
        projectList.clear();
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final Project project = new Project();
        project.setName("Test project name");
        project.setDescription("Test project description");
        project.setUserId(USER_ID_1);

        projectList.add(project);
        projectService.add(USER_ID_1, project);
        List<Project> actualProjectList = projectService.findAll();
        assertEquals(projectList, actualProjectList);

    }

    @Test(expected = UserIdEmptyException.class)
    public void testExceptionAdd() throws Exception {
        projectService.add("", new Project());
    }

    @Test
    public void testClear() throws Exception {
        assertNotEquals(0, projectRepository.findAllUserId(USER_ID_1).size());
        projectService.clear(USER_ID_1);
        assertEquals(0, projectRepository.findAllUserId(USER_ID_1).size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExceptionClear() throws Exception {
        projectService.clear("");
    }

    @Test
    public void TestfindAll() throws Exception {
        @NotNull final List<Project> projectsUser1 = new ArrayList<>();
        @NotNull final List<Project> projectsUser2 = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                projectsUser1.add(project);
            if (project.getUserId().equals(USER_ID_2))
                projectsUser2.add(project);
        }
        assertNotEquals(projectsUser1, projectsUser2);

        @NotNull final List<Project> actualProjectsUser1 = projectService.findAll(USER_ID_1);
        @NotNull final List<Project> actualProjectsUser2 = projectService.findAll(USER_ID_2);
        @NotNull final List<Project> actualProjectsAll = projectService.findAll();
        assertEquals(projectsUser1, actualProjectsUser1);
        assertEquals(projectsUser2, actualProjectsUser2);
        assertEquals(projectList, actualProjectsAll);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExceptionfindAll() throws Exception {
        projectService.findAll("");
    }

    @Test
    public void testFindOneById() throws Exception {
        for (@NotNull final Project project : projectList) {
            assertEquals(project, projectService.findOneById(project.getUserId(), project.getId()));
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testEmptyUserFindOneById() throws Exception {
        projectService.findOneById("", UUID.randomUUID().toString());
    }

    @Test(expected = IdEmptyException.class)
    public void testEmptyIdFindOneById() throws Exception {
        projectService.findOneById(UUID.randomUUID().toString(), "");
    }

    @Test
    public void testRemove() throws Exception {
        for (@NotNull final Project project : projectList) {
            assertTrue(projectRepository.existById(project.getUserId(), project.getId()));
            projectService.remove(project.getUserId(), project);
            assertFalse(projectRepository.existById(project.getUserId(), project.getId()));
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExceptionRemove() throws Exception {
        projectService.remove("", new Project());
    }

    @Test
    public void testRemoveById() throws Exception {
        for (@NotNull final Project project : projectList) {
            assertTrue(projectRepository.existById(project.getUserId(), project.getId()));
            projectService.removeById(project.getUserId(), project.getId());
            assertFalse(projectRepository.existById(project.getUserId(), project.getId()));
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testEmptyUserRemoveById() throws Exception {
        projectService.removeById("", UUID.randomUUID().toString());
    }

    @Test(expected = IdEmptyException.class)
    public void testEmptyIdRemoveById() throws Exception {
        projectService.removeById(UUID.randomUUID().toString(), "");
    }

    @Test
    public void testExistById() throws Exception {
        for (@NotNull final Project project : projectList) {
            assertTrue(projectService.existById(project.getUserId(), project.getId()));
        }
        assertFalse(projectService.existById(USER_ID_1, UUID.randomUUID().toString()));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUserEmptyExistById() throws Exception {
        projectService.existById("", UUID.randomUUID().toString());
    }

    @Test(expected = IdEmptyException.class)
    public void testEmptyIdExistById() throws Exception {
        projectService.existById(UUID.randomUUID().toString(), "");
    }

    @Test
    public void testGetSize() throws Exception {
        @NotNull final List<Project> projectsUser1 = new ArrayList<>();
        @NotNull final List<Project> projectsUser2 = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                projectsUser1.add(project);
            if (project.getUserId().equals(USER_ID_2))
                projectsUser2.add(project);
        }
        assertEquals(projectsUser1.size(), projectService.getSize(USER_ID_1));
        assertEquals(projectsUser2.size(), projectService.getSize(USER_ID_2));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExceptionGetSize() throws Exception {
        projectService.getSize("");
    }

    @Test
    public void testChangeProjectStatusById() throws Exception {
        int i = 0;
        for (@NotNull final Project project : projectList) {
            if (i % 2 == 0) {
                projectService.changeProjectStatusById(project.getUserId(), project.getId(), Status.IN_PROGRESS);
                @NotNull final Project actualProject = projectRepository.findOneById(project.getUserId(), project.getId());
                assertEquals(Status.IN_PROGRESS, actualProject.getStatus());
            }
            if (i % 3 == 0) {
                projectService.changeProjectStatusById(project.getUserId(), project.getId(), Status.COMPLETED);
                @NotNull final Project actualProject = projectRepository.findOneById(project.getUserId(), project.getId());
                assertEquals(Status.COMPLETED, actualProject.getStatus());
            }
            i++;
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testExceptionChangeProjectStatusById() throws Exception {
        projectService.changeProjectStatusById(UUID.randomUUID().toString(), "", Status.IN_PROGRESS);
    }

    @Test
    public void testCreate() throws Exception {
        @NotNull final String name = "TEST project";
        @NotNull final String description = "DEscription";
        @NotNull final Project project = projectService.create(USER_ID_1, name, description);
        @NotNull final Project actualProject = projectRepository.findOneById(project.getUserId(), project.getId());
        assertEquals(name, actualProject.getName());
        assertEquals(description, actualProject.getDescription());
        assertEquals(USER_ID_1, actualProject.getUserId());
        assertEquals(projectRepository.getSize(project.getUserId()), projectList.size() + 1);
    }

    @Test
    public void testUpdateById() throws Exception {
        for (@NotNull final Project project : projectList) {
            @NotNull final String name = project.getName() + "TEST";
            @NotNull final String description = project.getDescription() + "TEST";
            projectService.updateById(project.getUserId(), project.getId(), name, description);
            @NotNull final Project actualProject = projectRepository.findOneById(project.getUserId(), project.getId());
            assertEquals(name, actualProject.getName());
            assertEquals(description, actualProject.getDescription());
        }
    }

    @Test(expected = NameEmptyException.class)
    public void testExceptionUpdateById() throws Exception {
        projectService.updateById(UUID.randomUUID().toString(), UUID.randomUUID().toString(), "", "");
    }

}
